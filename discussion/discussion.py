# Python Control Structures
# [Section] Input
# input() is similar to prompt() in JS that seeks to gather data from user input. returns string data type
#  "\n" stands for line break
username = input("Please enter your name:\n")
print(f"Hi {username}! Welcome to Python Short Course!")

num1 = int(input("Please enter the first number \n"))
num2 = int(input("Please enter the second number \n"))
print(f"The sum of num1 and num2 is {num1 + num2}")

# [Section] if-else statement
test_num =  59
# in Python, the condition that the if statement has to assess is not enclosed in parenthesis. At the end of the statement, there is a colon, denoting that the indented statements below will be executed
# if we need to assess multiple conditions, use logical operators

# if test_num >= 60 and test_num == 0 :
	# print("Test Passed")

if test_num >= 60 :
	print("Test Passed")
else :
	print("Test Failed")

# if-else chains

test_num2 = int(input("Please enter a number for testing:\n"))
if test_num2 > 0:
	print("The number is positive")
elif test_num2 == 0:
	print("The number is zero")
else:
	print("The number is negative")



"""
Miniactivity
- create an if-else statement that determines if a number is divisible by 3,5, or both

- if it is divisible by 3, print "The number is divisible by 3"
- if it is divisible by 5, print "The number is divisible by 5"
- if it is divisible by 3 & 5, print "The number is divisible by 3 & 5"
- if it is not divisible by either 3 or 5, print "The number is not divisible by 3 nor 5"
"""

test_num3 = int(input("Please enter a number for testing:\n"))
if test_num3 % 3 == 0 and test_num3 % 5 == 0:
	print("The number is divisible by 3 & 5")
elif test_num3 % 3 == 0:
	print("The number is divisible by 3")
elif test_num3 % 5 == 0:
	print("The number is divisible by 5")
else:
	print("The number is not divisible by 3 nor 5")


# [Section] Loops

# [Section] While Loop
# performs a code block as long as the condition is True

i = 1
while i <= 5:
	print(f"Current Value: {i}")
	i += 1


# [Section] For Loop
# used to iterate through values or sequence
fruits = [ 	"apple", "orange", "banana" ]
for indiv_fruit in fruits:
	print(indiv_fruit)

# using range() method would allow us to iterate through values
# together with for loop, it will allow us to iterate through range of values

'''
SYNTAX:
	range(stop)
'''

# range_value = range(6)
# print(range_value)
for x in range(7):
	print(f"Current Value: {x}")

print("---")

for x in range(1, 10): 
#1st value = starting point
#2nd value=end point
	print(f"Current Value: {x}")

print("---")

for x in range(1, 10, 2): 
#1st value = starting point
#2nd value = end point
#3rd value = step value (increment)
	print(f"Current Value: {x}")



# [Section] Break and Continue Statements
# Break Statement
j = 1
while j < 6:
	print(j)
	if j == 3:
		break
	j += 1

print("---")
# Continue Statement
# is used to stop the current iteration and continue to the next iteration
# takes the control back to the top of the iteration
k = -1
while k < 6:
	k += 1 # signifies the top of the loop should we use continue statement
	if k == 3:
		continue
	print(k)
