input_year = int(input("Please input year:\n"))

if input_year % 4 == 0:
	if input_year % 100 == 0:
		if input_year % 400 == 0:
			print(f"{input_year} is a leap year")
		else:
			print(f"{input_year} is NOT a leap year")			
	else:
		print(f"{input_year} is a leap year")		

else:
	print(f"{input_year} is NOT a leap year")

input_col = int(input("Please input number of Columns:\n"))
input_row = int(input("Please input number of Rows:\n"))


for y in range(1, input_row+1):
	column = ""
	for x in range(1, input_col+1):
		column = column + "*"
		x += 1
	print(column)	
	y += 1
